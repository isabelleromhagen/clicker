package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Controller {

    @FXML
    public Label counterLabel;

    @FXML
    public void clickCount() {
         int count = (int) (Math.random()*10);
            counterLabel.setText(String.valueOf(count));
            if(count==10) count=0;

    }

}
